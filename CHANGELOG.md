# Changelog

## v0.0.2

- Base image: debian:12-slim

## v0.0.1

- Initial version
- Base image: debian:11-slim
