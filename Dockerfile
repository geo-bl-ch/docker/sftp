FROM debian:12-slim

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y \
        bash \
        lftp \
        openssh-client \
        corkscrew && \
    mkdir /app && \
    adduser --uid 1001 --ingroup root --shell /bin/bash --gecos "" --disabled-password app && \
    chgrp -R 0 /etc/passwd && \
    chmod -R g=u /etc/passwd && \
    chown -R 1001:0 /app && \
    chmod -R g=u /app && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

WORKDIR /app

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["bash"]

USER 1001
